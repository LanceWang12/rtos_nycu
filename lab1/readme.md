

# Lab1
[toc]

## Objective

To implement periodic tasks

To observe the scheduling behaviors

## Some script

```bash
./goToSource.bat 
# go to target directory
# Target directory is lab1/SOFTWARE/uCOS-II/lab1/BC45/TEST

# In lab1/SOFTWARE/uCOS-II/lab1/BC45/TEST
clean.bat # clean *.exe, ...
MAKETEST.BAT # a script to make test.exe
```

## compile & run:

1.goToSource.bat
2.clean.bat
3.maketest.bat
4.test.exe

## 修改的file

1. ./SOFTWARE/uCOS-II/lab1/BC45/SOURCE/test.C
2. ./SOFTWARE/uCOS-II/source/os_core.c :
   - osintexit
   - os_sched
   - osstart
   - ostimetick
3. ./SOFTWARE/uCOS-II/source/uCOS_II.H :
   - 最下面的define
   - os_tcb
